require 'spec_helper'
require 'facter'
require 'facter/appdata'

describe :appdata, type: :fact do
  subject(:fact) { Facter.fact(subject) }

  before :all do
    # perform any action that should be run for the entire test suite
  end

  before :each do
    # perform any action that should be run before every test
    Facter.clear
    # This will mock the facts that confine uses to limit facts running under certain conditions
    allow(Facter.fact(:kernel)).to receive(:value).and_return('windows')
    allow(ENV).to receive('APPDATA').and_return('C:\Users\tragiccode\AppData\Roaming')
  end

  it 'returns a value' do
    expect(Facter.fact(:appdata).value).to eq('c:\\windows\\appdata')
  end
end
