# Class: vscode
# ===========================
#
# Full description of class vscode here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# [*ensure*]
#   Ensure parameter passed onto Service[] resources.
#   Default: running
#
# @example
#    class { 'vscode':
#      package_ensure => 'present',
#    }
#
# Authors
# -------
#
# Author Name <michael@tragiccode.com>
#
# Copyright
# ---------
#
# Copyright 2017
#
class vscode(
  Enum['present', 'installed', 'absent'] $package_ensure = present,
  String $package_name = 'Microsoft Visual Studio Code',
  String $vscode_download_url,
  Stdlib::Absolutepath $vscode_download_absolute_path,
  Boolean $disable_extensions = false,
  Boolean $create_desktop_icon = true,
  Boolean $create_quick_launch_icon = true,
  Boolean $create_context_menu_files = true,
  Boolean $create_context_menu_folders = true,
  Boolean $add_to_path = true,
  Stdlib::Absolutepath $code_dir = extlib::path_join([$::facts[appdata], 'Code']),
  Stdlib::Absolutepath $code_user_dir = extlib::path_join([$code_dir, 'User']),
  Stdlib::Absolutepath $code_settings_file = extlib::path_join([$code_user_dir, 'settings.json']),
  Optional[Enum['vs-minimal', 'vs-seti']] $icon_theme = undef,
  Optional[String] $color_theme = undef,
) {

  contain vscode::install
  contain vscode::config

  Class['vscode::install']
  -> Class['vscode::config']

}
