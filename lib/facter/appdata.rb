require 'facter'

Facter.add(:appdata) do
  setcode do
    case Facter.value(:kernel)
    when 'windows'
      ENV.fetch('APPDATA', nil)
    when 'darwin'
      File.expand_path(File.join('~', 'Library', 'Application Support'))
    else
      File.expand_path('~')
    end
  end
end
